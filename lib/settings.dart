
import 'package:flutter/material.dart';

class Settings with ChangeNotifier{
  double fontSize = 16;
  double homeFontSize = 30;
  double appBarFontSize = 22;
  bool isDarkMode = false;
  getFontSize()=> fontSize;
  getDarkMode()=> isDarkMode;
  void changeDarkMode (isDarkMode){
    this.isDarkMode = isDarkMode;


    notifyListeners();


  }
  void decrement(){
    if (fontSize > 15 && appBarFontSize > 21 && homeFontSize > 29 ) {
      fontSize--;
      appBarFontSize--;
      homeFontSize--;
    }
    else {
      fontSize = fontSize;
      appBarFontSize =appBarFontSize;
      homeFontSize = homeFontSize;
    }
    notifyListeners();
  }
  void increment (){
    if (fontSize  > 35 && appBarFontSize > 36 && homeFontSize > 35 )
      {
        fontSize = fontSize;
        appBarFontSize = appBarFontSize;
        homeFontSize = homeFontSize;
      }
    else{
      fontSize++;
      appBarFontSize++;
      homeFontSize++;
    }
notifyListeners();

  }




}
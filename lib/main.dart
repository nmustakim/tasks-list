import 'dart:io';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:tasks_list_hive/settings.dart';
import 'homeScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Directory document = await getApplicationDocumentsDirectory();
  Hive.init(document.path);
  await Hive.openBox<String>("tasks");
  runApp( ChangeNotifierProvider(
    create: (_) => Settings(),
      child: MyApp()));
}

class MyApp extends StatelessWidget {
   const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    var themeProvider = Provider.of<Settings>(context);
    return MaterialApp(
      theme: themeProvider.getDarkMode() ? ThemeData.dark() : ThemeData.light(),
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: const HomeScreen(),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tasks_list_hive/settings.dart';

class HelpScreen extends StatefulWidget {
  const HelpScreen({Key? key}) : super(key: key);

  @override
  State<HelpScreen> createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<Settings>(context);
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          centerTitle: true,

          title:  Text("Help",style: TextStyle(fontSize: settings.appBarFontSize),),),

        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(

            crossAxisAlignment: CrossAxisAlignment.stretch,
            children:
            [
              const SizedBox(height: 40,),
              ListTile(leading:const Icon(Icons.circle,),title: Text("To add new task click on the + button below",style: TextStyle(fontSize: settings.fontSize),)),
              const SizedBox(height:20),
              ListTile(leading:const Icon(Icons.circle,
),title:  Text("To delete a task, long press on the tile of the task",style: TextStyle(fontSize: settings.fontSize)),),
              const SizedBox(height:20),
              ListTile(leading:const Icon(Icons.circle,),title:  Text("To update a task, press the tile of the task",style: TextStyle(fontSize: settings.fontSize)))


            ],
          ),
        ),
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tasks_list_hive/settings.dart';
import 'package:tasks_list_hive/settingsScreen.dart';

import 'settingsScreen.dart';

class AboutScreen extends StatelessWidget  {
    const AboutScreen({Key? key}) : super(key: key);




  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<Settings>(context);


    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          centerTitle: true,

          title:  Text("About",style: TextStyle(fontSize: settings.appBarFontSize),),),

        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: SingleChildScrollView(
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.stretch,
              children:
            [
                SizedBox(height: 40,),
                ListTile(title: Text("This is a simple task list app.\n\nYou can add a new task.\n\nYou can update the task that has already been written.\n\nYou also can delete the task that you have added to the task list easily",style: TextStyle(letterSpacing:5,fontSize: settings.fontSize),)),
                SizedBox(height:20),

              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tasks_list_hive/settings.dart';
import 'package:theme_provider/theme_provider.dart';

class SettingsScreen extends StatefulWidget  {
  const SettingsScreen({Key? key}) : super(key: key);




  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen>  {


 @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override





  @override
  Widget build(BuildContext context) {
    var themeProvider =  Provider.of <Settings> (context);
    final settings = Provider.of<Settings>(context);
    return SafeArea(
      child: Scaffold(

          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.black,
            title:  Text("Settings",style: TextStyle(fontWeight: FontWeight.bold,fontSize:settings.fontSize)),),
          body: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                 ListTile(title: Text("FontSize",style: TextStyle(fontWeight: FontWeight.bold,fontSize: settings.fontSize),),),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    children:
                      [
                      const Text("Increase",),Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: InkWell(
                        onTap: (){
                          settings.increment();




                        },
                          child: const Icon(Icons.add_circle)),

                    ),Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: InkWell(
                        onTap: (){
                          settings.decrement();



                        },
                          child: const Icon(Icons.remove_circle)),
                    ),const Text("Decrease")
                    ],),
                ),
              ListTile(title: Text("Theme",style: TextStyle(fontWeight: FontWeight.bold,fontSize: settings.fontSize),),),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    children: [
                      const Text("Day"),Switch(
                        inactiveThumbColor: Colors.black,
                           activeColor: Colors.white,
                          value: themeProvider.getDarkMode(), onChanged:(value){
                            setState((){
                              themeProvider.changeDarkMode(value);

                            });


                      },
                      ),const Text("Night")
                    ],
                  ),
                )
              ],
            ),
          )



      ),
    );

  }

}
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';
import 'package:tasks_list_hive/settingsScreen.dart';

import 'aboutScreen.dart';
import 'helpScreen.dart';
import 'settings.dart';

class TasksScreen extends StatefulWidget {
   const TasksScreen({Key? key}) : super(key: key);




  @override
  State<TasksScreen> createState() => _TasksScreenState();
}

class _TasksScreenState extends State<TasksScreen> {


  late Box<String> tasksBox;


  TextEditingController textEditingController1 = TextEditingController();
  TextEditingController textEditingController2 = TextEditingController();

  @override
  void initState() {
    super.initState();
    tasksBox = Hive.box<String>("tasks");
  }

  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<Settings>(context);
    return Scaffold(

        appBar: AppBar(
          backgroundColor: Colors.black,

            centerTitle: true,
            title:  Text("Tasks List",style: TextStyle(fontSize: settings.appBarFontSize),),
            actions: [
              PopupMenuButton(

                  itemBuilder: (context) {
                    return [
                      const PopupMenuItem(
                        value: 0,
                        child: Text("About"),
                      ),
                      const PopupMenuItem(
                        value: 1,
                        child: Text("Help"),
                      ),
                      const PopupMenuItem(
                        value: 2,
                        child: Text("Settings"),
                      ),
                    ];

                  },
                  onSelected: (value) {
                    if (value == 0) {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => AboutScreen()));
                    }
                    else if (value == 1) {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => HelpScreen()));
                    }
                    else {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => const SettingsScreen()));
                    }
                  })
            ]),
        body: Center(
          child: Container(

              child: ValueListenableBuilder(
                  valueListenable: tasksBox.listenable(),
                  builder: (context, Box<String> tasks, _) {
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        final name = tasks.keys.toList()[index];
                        final description = tasks.get(name);
                        return SingleChildScrollView(
                          child: ListTile(
                            tileColor: Colors.lightGreen,



                              onTap: () {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                          title: const Text("Tasks form"),
                                          actions: [
                                            ElevatedButton.icon(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                icon: const Icon(Icons.close),
                                                label: const Text("Cancel")),
                                            ElevatedButton.icon(
                                                onPressed: () {
                                                  final name =
                                                      textEditingController1
                                                          .text;
                                                  final description =
                                                      textEditingController2
                                                          .text;

                                                  tasksBox.put(
                                                      name, description);
                                                  Navigator.pop(context);
                                                },
                                                icon: const Icon(Icons.save),
                                                label: const Text("Save")),
                                          ],
                                          content: SingleChildScrollView(
                                              child: Column(
                                                children: [
                                                  TextField(
                                                    controller:
                                                    textEditingController1,
                                                    decoration:
                                                    const InputDecoration(

                                                        hintText:
                                                        "Name of the task",
                                                        labelText: "Name",),
                                                  ),
                                                  TextField(
                                                    controller:
                                                    textEditingController2,
                                                    decoration: const InputDecoration(
                                                        hintText:
                                                        "Write a description",
                                                        labelText: "Description"),
                                                  )
                                                ],
                                              )));
                                    });
                              },
                              onLongPress: () {
                                tasksBox.delete(name);
                              },
                              title: Text(name),
                              subtitle: Text(description!)),
                        );
                      },
                      itemCount: tasks.keys.toList().length,
                    );
                  })),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          onPressed: () {
            textEditingController1.clear();
            textEditingController2.clear();

            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                      title: const Text("Tasks form"),
                      actions: [
                        ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                                primary: Colors.lightGreen),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: const Icon(
                              Icons.work_off_outlined,
                              color: Colors.black,
                            ),
                            label: const Text(
                              "Cancel",
                              style: TextStyle(color: Colors.black),
                            )),
                        ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                                primary: Colors.lightGreen),
                            onPressed: () {
                              var name = textEditingController1.text;
                              var description = textEditingController2.text;

                              tasksBox.put(name, description);
                              Navigator.pop(context);
                            },
                            icon: const Icon(
                              Icons.save,
                              color: Colors.black,
                            ),
                            label: const Text(
                              "Save",
                              style: TextStyle(color: Colors.black),
                            )),
                      ],
                      content: SingleChildScrollView(
                          child: Column(
                            children: [
                              TextField(
                                controller: textEditingController1,
                                decoration: const InputDecoration(
                                    hintText: "Name of the task",
                                    labelText: "Name"),
                              ),
                              TextField(
                                controller: textEditingController2,
                                decoration: const InputDecoration(
                                    hintText: "Write a description",
                                    labelText: "Description"),
                              )
                            ],
                          )));
                });
          },
          child: const Icon(Icons.add),
        ));
  }
}

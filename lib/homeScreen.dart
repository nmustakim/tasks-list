import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tasks_list_hive/aboutScreen.dart';
import 'package:tasks_list_hive/helpScreen.dart';
import 'package:tasks_list_hive/settings.dart';
import 'package:tasks_list_hive/settingsScreen.dart';
import 'package:tasks_list_hive/tasksScreen.dart';


class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<Settings>(context);
    return SafeArea(
      child:Scaffold(
            body: Container(
                color: Colors.black,

                child: Stack(
                  children: [

                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) =>  const TasksScreen()));
                              },
                              child: Container(

                               color: Colors.white,

                                width: MediaQuery.of(context).size.width / 2.03,
                                height: MediaQuery.of(context).size.height / 2.1,

                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children:  [
                                    Icon(Icons.list,size: 50,),
                                    Center(child: Text("Tasks List",style: TextStyle(color: Colors.black,fontSize: settings.homeFontSize,fontWeight: FontWeight.bold),)),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>  const SettingsScreen()));
                              },
                              child: Container(
                                color: Colors.white,
                                width: MediaQuery.of(context).size.width / 2.03,
                                height: MediaQuery.of(context).size.height / 2.1,

                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children:  [

                                    Icon(Icons.settings,size: 50,),
                                    Center(child: Text("Settings",style: TextStyle(color: Colors.black,fontSize: settings.homeFontSize,fontWeight: FontWeight.bold),)),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) =>  const HelpScreen()));
                              },
                              child: Padding(padding: const EdgeInsets.fromLTRB(0, 1.5,0, 1.5),
                                child: Container(

                                  width: MediaQuery.of(context).size.width / 2.03,
                                  height: MediaQuery.of(context).size.height / 2.1,
                                  color: Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.help,size: 50,),
                                      Center(child: Text("Help",style: TextStyle(color: Colors.black,fontSize: settings.homeFontSize,fontWeight: FontWeight.bold),)),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) =>  const AboutScreen()));
                                },
                                child: Container(

                                  width: MediaQuery.of(context).size.width / 2.03,
                                  height: MediaQuery.of(context).size.height / 2.1,
                                  color: Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.description,size: 50,),
                                      Center(child: Text("About",style: TextStyle(color: Colors.black,fontSize: settings.homeFontSize,fontWeight: FontWeight.bold),)),
                                    ],
                                  ),
                                ))
                          ],
                        )
                      ],
                    ),
                    Center(child: Container(height: 145,width: 145,color:   Colors.white,)),
                    const Center(child: Icon(Icons.list_alt,size: 200,color: Colors.black)),
                  ],
                )

            ),
          ),
    );
  }
}
